
## ROOT & XRootD exercise - Solution

The directory **XRootD_container** holds the ``Dockerfile`` necessary to build the image for the **XRootD** server. 

```bash
cd XRootD_container
docker build -t "<usr_name>/xrootd_img> .
...
```

The directory **ROOT_container** holds the ``Dockerfile`` and any additional files necessary to build the image for the **ROOT** application.

