#include "TFile.h"
#include "TH1F.h"
#include "TRandom.h"
#include "TRandom3.h"
#include <iostream>
#include <string>

int main(int argc, char* argv[])
{
  gRandom = new TRandom3(0);
  std::cout << "Generating historgram" << std::endl;
  // Try to save the file in the XRootD server running on
  // localhost:1094
  std::string fn = "histo.root";
  TFile* f = TFile::Open(fn.c_str(), "recreate");

  if (!f)
  {
    std::cerr << "Failed to open file: " << fn << std::endl;
    exit(1);
  }

  TH1F h1("hgaus","GridKa - Histogram from a gaussian ",100,-3,3);
  h1.FillRandom("gaus",10000);
  h1.Write();
  delete f;
  return 0;
}
